import { useEffect, useState } from "react";
import { useLocation } from "wouter";
import { useCartContext } from "../contexts/CartContext";

export type Product = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  rating: { rate: number; count: number };
  title: string;
};

export default function Products() {
  const [_, setLocation] = useLocation();
  const [products, setProducts] = useState<Product[]>([]);
  const { dispatch } = useCartContext();

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setProducts(json));
  }, []);

  return (
    <>
      <h2 style={{textAlign: "center", fontSize: "30px", marginTop: "50px"}}>Products</h2>
  
      <div style={{ display: "flex", flexWrap: "wrap", columnGap: "30px" }}>
        {products.map((product) => (
          <div
            key={product.id}
            style={{
              margin: "20px",
              padding: "8px",
              width: "185px",
              border: "2px solid",
              borderColor: "#020321",
              borderRadius: "10px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <div style={{ width: "100%", height: "200px", marginBottom: "10px" }}>
              <img
                style={{ width: "100%", height: "100%", objectFit: "contain" }}
                src={product.image}
                alt={product.title}
              />
            </div>
            <div style={{ flexGrow: 1, display: "flex", flexDirection: "column", alignItems: "center" }}>
              <p style={{ fontSize: ".8rem", textAlign: "center"}}>
                <strong>{product.title}</strong>
              </p>
              <p style={{  }}>${product.price}</p>
            </div>
            <div style={{ width: "100%" }}>
              <button
                style={{ width: "100%" }}
                onClick={() => {
                  setLocation(`/products/${product.id}`);
                }}
              >
                View
              </button>
              <div style={{ margin: "8px" }} />
              <button
                style={{
                  width: "100%",
                  backgroundColor: "#020321",
                  color: "white",
                }}
                onClick={() => {
                  dispatch({ type: "addToCart", product });
                }}
              >
                Add to Cart
              </button>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

import { Link } from "wouter";
import { BsCart } from "react-icons/bs";
import { useCartContext } from "../contexts/CartContext";
import { useState } from "react";

export default function Cart() {
  const { cart, dispatch } = useCartContext();
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  function calculateTotalAmount() {
    let total = 0;
    for (let i = 0; i < cart.length; i++) {
      const product = cart[i];
      total += product.price * product.quantity;
    }
    return total.toFixed(2);
  }

  return (
    <>
      <Link
        style={{ margin: "20px", fontSize: "26px" }}
        onClick={toggleSidebar}
        href="/"
      >
        <BsCart />
        {cart.length}
      </Link>
      {isSidebarOpen && (
        <div
          style={{
            width: "500px",
            height: "100%",
            position: "fixed",
            top: 0,
            right: 0,
            background: "white",
            boxShadow: "0 0 8px rgba(0, 0, 0, 0.2)",
            paddingLeft: "16px",
            paddingRight: "16px",
            zIndex: 9999,
            color: "black",
            overflowY: "auto",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              marginBottom: "8px",
            }}
          >
            <h2>My Cart</h2>
            <button
              style={{ border: "none" }}
              onClick={toggleSidebar}
            >
              X
            </button>
          </div>
          {cart.length > 0 ? (
            cart.map((cartItem) => {
              const totalPrice = cartItem.price * cartItem.quantity;
              return (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginBottom: "8px",
                    borderBottom: "1px solid",
                    borderColor: "#020321",
                    paddingBottom: "10px"
                  }}
                  key={cartItem.id}
                >
                  <img
                    src={cartItem.image}
                    alt={cartItem.title}
                    style={{ width: "100px", height: "100px", marginRight: "20px", objectFit: "contain", minWidth: "100px" }}
                  />
                  <div style={{ flexGrow: 1, marginRight: "50px" }}>
                    <p>{cartItem.title}</p>
                    <p>Amount: ${totalPrice}</p>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      
                      <button
                        style={{ padding: "8px", fontSize: "16px" }}
                        onClick={() => {
                          dispatch({ type: "increaseQuantity", productId: cartItem.id });
                        }}
                      >
                        +
                      </button>
                      <p style={{ margin: "10px 10px", fontSize: "16px" }}>
                        {cartItem.quantity}
                      </p>
                      
                      <button
                        style={{ padding: "8px", fontSize: "16px" }}
                        onClick={() => {
                          dispatch({ type: "decreaseQuantity", productId: cartItem.id });
                        }}
                      >
                        -
                      </button>
                    </div>
                  </div>
                  <button
                    style={{ marginLeft: "20px", padding: "10px", color: "red", borderColor: "red" }}
                    onClick={() => {
                      dispatch({ type: "removeFromCart", productId: cartItem.id });
                    }}
                  >
                    X
                  </button>
                </div>
              );
            })
          ) : (
            <p style={{ textAlign: "center" }}>Your cart is empty.</p>
          )}
  
          <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", marginTop: "16px" }}>
            <p style={{ fontWeight: "bold"}}>Total Amount: ${calculateTotalAmount()}</p>
            <button
              style={{ color: "red", borderColor: "red" }}
              onClick={() => dispatch({ type: "clearCart" })}
            >
              Clear Cart
            </button>
          </div>
        </div>
      )}
    </>
  );
  
  
}

import { FunctionComponent, useEffect, useState } from "react";
import { RouteComponentProps } from "wouter";
import { Product } from "./Products";
import { useCartContext } from "../contexts/CartContext";

const ViewProduct: FunctionComponent<RouteComponentProps> = ({ params }) => {
  const { dispatch } = useCartContext();
  const [product, setProduct] = useState<Product | undefined>();

  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${params.productId}`)
      .then((res) => res.json())
      .then((json) => setProduct(json));
  }, [params.productId]);

  if (!product) {
    return <p>Loading...</p>;
  }

  const addToCart = () => {
    dispatch({ type: 'addToCart', product });
  };

  return (
    <>
  <h2 style={{textAlign: "center", fontSize: "30px", marginTop: "50px", marginBottom: "50px"}}>View Product</h2>
  <div 
    style={{ 
      display: "flex", 
      marginLeft: "150px", 
      marginRight: "150px", 
      padding: "20px",
      border: "2px solid",
      borderColor: "#020321",
      borderRadius: "10px" }}>
    <div style={{ flex: 1 }}>
      <h3>{product.title}</h3>
      <p>Price: ${product.price}</p>
      <p style={{ textAlign: "justify", textIndent: "20px"}}>{product.description}</p>
      <div style={{textAlign: "center", justifyContent: "center"}}>
        <button style={{ backgroundColor: "#020321", color: "white", marginRight: "50px"}} onClick={addToCart}>Add to Cart</button>
      </div>
    </div>
    <img src={product.image} alt={product.title} style={{ marginLeft: "150px", maxWidth: "250px"}} />
  </div>
</>
  );
};

export default ViewProduct;
import { Link, Route, Switch } from "wouter";
import Products from "./pages/Products";
import ViewProduct from "./pages/ViewProduct";
import { CartProvider } from "./contexts/CartContext";
import Cart from "./pages/Cart";
import { useEffect, useState } from "react";

function App() {
  const [isFooterVisible, setIsFooterVisible] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const { scrollTop, clientHeight, scrollHeight } = document.documentElement;
      const isAtBottom = scrollTop + clientHeight === scrollHeight;
      setIsFooterVisible(isAtBottom);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <CartProvider>
        <header>
          <div style={{ display: "flex"}}>
            <Link style={{margin: "20px"}} href="/">Home</Link>
            <div style={{ flexGrow: 1 }} />
            <Cart />
          </div>
        </header>
        <div style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
          <Switch>
            <Route path="/cart" component={Cart} />
            <Route path="/products/:productId" component={ViewProduct} />
            <Route path="/" component={Products} />
          </Switch>
        </div>

        <footer
          style={{
            opacity: isFooterVisible ? 1 : 0,
            transition: "opacity 0.5s ease",
          }}
        >
          © 2023 Mock Ecommerce. All rights reserved.
        </footer>
      </CartProvider>
    </>
  );
}

export default App;
